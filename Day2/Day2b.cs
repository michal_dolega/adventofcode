using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Day2
{
    public class Day2b
    {
        private string _filename = @"Day2.txt";

        private bool solved = false;
        
        public void Solve()
        {
            var filepath = Path.Combine(Directory.GetCurrentDirectory(), this._filename);

            var ids = new List<string>();

            using (var sr = File.OpenText(filepath))
            {
                while (!sr.EndOfStream)
                {
                    ids.Add(sr.ReadLine());
                }
            }

            foreach (var id in ids)
            {
                foreach (var element in ids)
                {
                    if (compareTwoStrings(id, element) && !solved)
                    {
                        RemoveNotRepeatingLetters(id, element);
                    }
                }
            }
        }

        private void RemoveNotRepeatingLetters(string first, string second)
        {
            var result = "";
            for (int i = 0; i < first.Length; i++)
            {
                if (first[i] == second[i])
                {
                    result += first[i];
                }
            }
            
            Console.WriteLine("the answer for second part of Day 2 Challenge is " +result);
            this.solved = true;
        }

        private bool compareTwoStrings(string left, string right)
        {
            if (left == right)
                return false;
                
            int differencesCounter = 0;

            for (int i = 0; i < left.Length; i++)
            {
                if (left[i] != right[i])
                    differencesCounter++;
                if (differencesCounter > 2)
                    return false;
            }

            return differencesCounter == 1;
        }
    }
}