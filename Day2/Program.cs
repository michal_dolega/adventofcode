﻿using System;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var twoA = new Day2a();
            twoA.Solve();

            var twoB = new Day2b();
            twoB.Solve();
        }
    }
}