using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Day2
{
    public class Day2a
    {
        private string _filename = @"Day2.txt";

        public void Solve()
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), _filename);

            var lines = new List<string>();
            
            using (var sr = File.OpenText(filePath))
            {
                while (!sr.EndOfStream)
                {
                    lines.Add(sr.ReadLine());
                }
            }
    
            var sortedLines = lines.Select(l => string.Concat(l.OrderBy(c => c))).ToList();
            
            var charCounter = new Dictionary<char, int>()
            {
                {'2', 0},
                {'3', 0}
            };

            var charRepetitionList = sortedLines.Select(l => Regex.Matches(l, @"(\w)\1+").ToList()).ToList();

            foreach (var repetitionList in charRepetitionList)
            {
                if (repetitionList.Any(l => l.Length == 2))
                    charCounter['2']++;
                if (repetitionList.Any(l => l.Length == 3))
                    charCounter['3']++;
            }

            Console.WriteLine("The answer for first part of Day 2 challenge is " + charCounter['2'] * charCounter['3']);
        }

        private bool RepeatTwoTimes(string line)
        {
            return Regex.IsMatch(line, @"(\w)(\1)");
        }

        private bool RepeatThreeTimes(string line)
        {
            return Regex.IsMatch(line, @"(\w)(\1){2}");
        }
    }
}