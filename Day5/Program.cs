﻿using System;
using System.IO;
using System.Linq;

namespace Day5
{
    class Program
    {
        static void Main(string[] args)
        {

            FirstPart();
            SecondPart();

        }

        public static void SecondPart()
        {
            string result = "";

            using (StreamReader sr = File.OpenText(Path.Combine(Directory.GetCurrentDirectory(), "Day5.txt")))
            {
                while (!sr.EndOfStream)
                {
                    var currentChar = (char) sr.Read();
                    if (result.Length > 0 && ToggleCapitalization(currentChar) == result.Last())
                    {
                        result = result.Remove(result.Length - 1);
                    }
                    else
                        result += currentChar;
                }
            }

            Console.WriteLine("The letter with smallest number is correct solution of Day 5 problem - part 2");
            foreach (var letter in "abcdefghijklmnopqrstuvwxyz")
            {
                var tempResult = result;

                tempResult = tempResult.Replace(letter.ToString(), "");
                tempResult = tempResult.Replace(char.ToUpper(letter).ToString(), "");

                Console.WriteLine($"{letter}: {ProcessString(ProcessString(tempResult)).Length}");
            }
            
        }

        public static string ProcessString(string str)
        {
            var tempstr = "";

            bool changes = false;

            changes = false;
            foreach (var character in str)
            {
                if (tempstr.Length > 0 && ToggleCapitalization(character) == tempstr.Last())
                {
                    tempstr = tempstr.Remove(tempstr.Length - 1);
                    changes = true;
                }
                else
                    tempstr += character;
            }

            return tempstr;
        }

        public static char ToggleCapitalization(char character)
        {
            return char.IsLower(character) ? char.ToUpper(character) : char.ToLower(character);
        }
        
        public static void FirstPart()
        {
            string result = "";

            using (StreamReader sr = File.OpenText(Path.Combine(Directory.GetCurrentDirectory(), "Day5.txt")))
            {
                while (!sr.EndOfStream)
                {
                    var currentChar = (char) sr.Read();
                    if (result.Length > 0 && ToggleCapitalization(currentChar) == result.Last())
                    {
                        result = result.Remove(result.Length - 1);
                    }
                    else
                        result += currentChar;
                }
            }

            Console.WriteLine($"The answer for Day5 first problem is: {result.Length}");
        }
    }
}