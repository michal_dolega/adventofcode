using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day4
{
    public class Parser
    {
        public string Filepath => Path.Combine(Directory.GetCurrentDirectory(), "Day4.txt");

        public List<Shift> GetShiftsFromFile()
        {
            var input = FetchInputAsList();
            var splitedList = input.Select(SplitTimeStampAndAction).ToList();
            var sortedList = splitedList.OrderBy(l => l.Item1).ToList();
            
            return GenerateShifts(sortedList);
        }

        public List<Shift> GenerateShifts(List<Tuple<DateTime, string>> actions)
        {
            var shifts = new List<Shift>();

            var startShiftPattern = @"Guard (#\d+) begins shift";
            var startNapPattern = "falls asleep";
            var endNapPattern = "wakes up";

            Shift currentShift = null;
            DateTime? currentNapStart = null;

            var currentWatchmanNaps = new List<Tuple<DateTime, DateTime>>();

            foreach (var action in actions)
            {
                var match = Regex.Match(action.Item2, startShiftPattern);
                if (match.Success)
                {
                    if (currentShift != null)
                    {
                        shifts.Add(currentShift);
                    }
                    currentShift = new Shift(match.Groups[1].Value);
                }
                else if (action.Item2 == startNapPattern)
                {
                    currentNapStart = action.Item1;
                }
                else if (action.Item2 == endNapPattern)
                {
                    currentShift.Naps.Add(Tuple.Create(currentNapStart.Value, action.Item1));
                }
            }

            return shifts;
        }

        private Tuple<DateTime, string> SplitTimeStampAndAction(string line)
        {
            var pattern = @"^\[(.+)\]\s+(.+)$";

            var match = Regex.Match(line, pattern);

            var timeStampString = match.Groups[1].Value;
            var action = match.Groups[2].Value;

            DateTime.TryParseExact(timeStampString, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out var timeStamp);

            return Tuple.Create(timeStamp, action);
        }

        private List<string> FetchInputAsList()
        {
            var inputLines = new List<string>();

            using (StreamReader sr = File.OpenText(Filepath))
            {
                while (!sr.EndOfStream)
                    inputLines.Add(sr.ReadLine());
            }

            return inputLines;
        }
    }

    public class Shift
    {
        public string Id { get; }
        public List<Tuple<DateTime, DateTime>> Naps { get; }

        public Shift(string id)
        {
            Id = id;
            Naps = new List<Tuple<DateTime, DateTime>>();
        }
    }
}