﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new Parser();
            var shifts = parser.GetShiftsFromFile();

            //This challenge should be improved by used of Dictionary<int, int> as representation of minutes slept by guards.

            var betterStructuredWorkers = shifts.GroupBy(s => s.Id,
                s => s.Naps,
                (id, naps) => new {Key = id, Naps = naps.SelectMany(t => t.ToArray()).ToList()}).ToList();

            var bestStructured =
                betterStructuredWorkers.Select(t => new {Key = t.Key, Naps = t.Naps, minutes = Minutes(t.Naps)})
                    .ToList();

            //First part solution
            
            var mostSleepy =
                bestStructured.Find(worker => worker.minutes.Sum() == bestStructured.Max(w => w.minutes.Sum()));

            Console.WriteLine($"First part of challenge. The most sleeping guard {mostSleepy.Key}. He was sleeping {mostSleepy.minutes.Sum()} minutes. Solution (863 * 46 = 39698) must be taken from debugger due to representation of minutes as array indexes.");
            
            //Second part solution
            
            var mostMinutes = bestStructured.Max(t => t.minutes.Max());

            var sleeperGuardian = bestStructured.Find(obj => obj.minutes.Max() == mostMinutes);
            
            Console.WriteLine($"Second part of challenge. The most sleeping guard {sleeperGuardian.Key}. He was sleeping {mostMinutes} times at one specific moment (minute 40. Solution (373 * 40 = 14920) must be taken from debugger due to representation of minutes as array indexes.");
            
        }

        public static int[] Minutes(List<Tuple<DateTime, DateTime>> napsTuples)
        {
            var minutes = new int[60];

            foreach (var tuple in napsTuples)
            {
                var begin = tuple.Item1.Minute;
                var end = tuple.Item2.Minute;

                var minutesAsleep = Enumerable.Range(begin, end - begin).ToArray();

                foreach (var minute in minutesAsleep)
                {
                    minutes[minute]++;
                }
            }
            return minutes;
        }
    }
}