﻿using System;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            var threeA = new Day3a();
            threeA.Solve(true);
            
            var threeB = new Day3b();
            threeB.Solve();

        }
    }
}