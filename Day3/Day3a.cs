using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Day3
{
    public class Day3a
    {
        public int[,] Fabric => this._Fabric;
        public List<Claim> Claims => this._claims;
        
        private List<Claim> _claims = new List<Claim>();

        private int[,] _Fabric =  new int[1000, 1000];

        private string claimRegex = @"^#(\d+)\s@\s(\d+),(\d+):\s+(\d+)x(\d+)$";

        private string _filename = "Day3.txt";

        public Claim GenerateClaimFromString(string input)
        {
            var match = Regex.Match(input, claimRegex);

            var args = match.Groups
                .Skip(1).Select(g =>
                {
                    int.TryParse(g.Value, out var i);
                    return i;
                }).ToArray();

            var arguments = Tuple.Create(args[0], args[1], args[2], args[3], args[4]);

            return new Claim(arguments.Item1, arguments.Item2, arguments.Item3, arguments.Item4, arguments.Item5);
        }

        public void CoverFabric(Claim claim)
        {
            for (int i = claim.LeftEdge; i < claim.LeftEdge + claim.Width; i++)
            {
                for (int j = claim.TopEdge; j < claim.TopEdge + claim.Height; j++)
                {
                    Fabric[j, i]++;
                }
            }
        }

        public void Solve(bool flag)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), this._filename);

            
            using (StreamReader sr = File.OpenText(filePath))
            {
                while (!sr.EndOfStream)
                {
                    var claim = GenerateClaimFromString(sr.ReadLine());
                    this.Claims.Add(claim);
                }
            }

            foreach (var claim in Claims)
            {
                this.CoverFabric(claim);
            }
            if(flag)
                Console.WriteLine($"Solution of Day 3 Challenge part 1. Result of Count Overlap: {CountOverlap()}");
        }

        public int CountOverlap()
        {
            var overlappingInches = 0;
            for (int i = 0; i < Fabric.GetUpperBound(0); i++)
            {
                for (int j = 0; j < Fabric.GetUpperBound(1); j++)
                {
                    if (Fabric[i, j] > 1)
                        overlappingInches++;
                }
            }
            return overlappingInches;
        }

    }


    public struct Claim
    {
        public int TopEdge { get; set; }
        public int LeftEdge { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Id { get; set; }

        public Claim(int id, int leftEdge, int topEdge, int width, int height)
        {
            this.Id = id;
            this.TopEdge = topEdge;
            this.LeftEdge = leftEdge;
            this.Width = width;
            this.Height = height;
        }
    }
}