using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day3
{
    public class Day3b
    {

        public void Solve()
        {
            var day3aSolution = new Day3a();
            day3aSolution.Solve(false);

            foreach (var claim in day3aSolution.Claims)
            {
                var isOverlapped = false;
                
                for (int i = claim.LeftEdge; i < claim.LeftEdge + claim.Width; i++)
                {
                    for (int j = claim.TopEdge; j < claim.TopEdge + claim.Height; j++)
                    {
                        if (day3aSolution.Fabric[j, i] > 1)
                        {
                            isOverlapped = true;
                        }        
                    }
                }

                if (!isOverlapped)
                    Console.WriteLine($"Solution od Day 3 challenge part 2. The only one not overlapped is: {claim.Id}");

            }
        }
    }
}