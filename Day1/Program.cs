﻿using System;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            var oneA = new Day1a();
            oneA.Solve();
            
            var oneB = new Day1b();
            oneB.Solve();
        }
    }
}