using System;
using System.IO;

namespace Day1
{
    public class Day1a
    {
        private string _fileName = @"Day1.txt";
        private int _sum = 0;
        
        public void Solve()
        {
            var pathToFile = Path.Combine(Directory.GetCurrentDirectory(), this._fileName);
            var str = Directory.GetCurrentDirectory();
            using (StreamReader sr = File.OpenText(pathToFile))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if(int.TryParse(line, out var number))
                        _sum += number;
                }
            }
            
            Console.WriteLine("The answer for first part of Day 1 task is " + _sum);
        }
    }
}