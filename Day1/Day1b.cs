using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace Day1
{
    public class Day1b
    {
        private string _fileName = @"Day1.txt";
        private int _frequencyState;
        private List<int> _frequenciesRecord;
        private bool isRepetitionFound = false;

        public event EventHandler<FrequencyEventArgs> FrequencyRepeated;

        protected virtual void OnFrequencyRepeated(FrequencyEventArgs e)
        {
            FrequencyRepeated?.Invoke(this, e);
        }

        private string PathToFile => Path.Combine(Directory.GetCurrentDirectory(), _fileName);

        public Day1b()
        {
            _frequenciesRecord = new List<int> {0};
            _frequencyState = 0;
            FrequencyRepeated += AnnounceFrequencyRepetition;
        }


        public void Solve()
        {
            while (!isRepetitionFound)
            {
                using (var sr = File.OpenText(PathToFile))
                {
                    while (!sr.EndOfStream && !isRepetitionFound)
                    {
                        var line = sr.ReadLine();
                        if (int.TryParse(line, out var frequency))
                        {
                            this.UpdateFrequencies(frequency);
                        }
                    }
                }
            }
        }

        private void UpdateFrequencies(int number)
        {
            this._frequencyState += number;
            if (_frequenciesRecord.Contains(this._frequencyState))
            {
                OnFrequencyRepeated(new FrequencyEventArgs(this._frequencyState));
                isRepetitionFound = true;
                
            }
            else
            {
                this._frequenciesRecord.Add(_frequencyState);
            }
            
        }

        private void AnnounceFrequencyRepetition(object sender, FrequencyEventArgs frequency)
        {
            Console.WriteLine($"Answer for second part of Day 2 challenge. Frequency repeated in state of {frequency.Value}");
        }
    }

    public class FrequencyEventArgs : EventArgs
    {
        public int Value { get; }

        public FrequencyEventArgs(int value)
        {
            Value = value;
        }
    }
}